/**
 *
 *  Web Starter Kit
 *  Copyright 2014 Google Inc. All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 *
 */

'use strict';

// Include Gulp & Tools We'll Use
var gulp = require('gulp');
var rupture = require('rupture');
var $ = require('gulp-load-plugins')();
var stylus = require('gulp-stylus');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var autoprefixer = require('gulp-autoprefixer');
var AUTOPREFIXER_BROWSERS = ['last 2 version'];

// Compile and Automatically Prefix Stylesheets
gulp.task('styles', function () {
  gulp.src('./app/stylus/style.styl')
  .pipe(
    stylus({
      compress: true,
      use: [rupture()]
    })
    )
  .pipe(autoprefixer())
  .pipe(gulp.dest('./app/styles'))
});

// Watch Files For Changes & Reload
gulp.task('serve', ['styles'], function () {
  browserSync({
    notify: true,
    // Customize the BrowserSync console logging prefix
    logPrefix: 'Romagnoli',
    // Run as an https by uncommenting 'https: true'
    // Note: this uses an unsigned certificate which on first access
    //       will present a certificate warning in the browser.
    // https: true,
    server: ['app']
  });

  gulp.watch(['app/**/*.html'], reload);
  gulp.watch(['app/stylus/style.styl'], ['styles', reload]);
  gulp.watch(['app/scripts/**/*.js'], reload);
  gulp.watch(['app/images/**/*'], reload);
});
